## Custom Themes
##### ***Every theme is hated by somebody***<br><br>

- *Are you blinded by light themes?*
- *Is it hard to see anything in dark themes?*
- *Do you want things to be more colorful?*

There are three basic themes in Fedilab. Dark, Light and Black. You can use one of them. However if you're not satisfied with these built-in themes, you can customize them. Fedilab allows you to change colors in most parts of the app.

- [Get custom themes](../../../themes)
- Continue reading this tutorial

#### How to change the base theme

- Open the the navigation drawer and select settings<br><br>
<img src="../../res/custom_themes/1.gif" height="240px">

- Go to 'THEMING' tab and tap on the 'Themes' option<br><br>
<img src="../../res/custom_themes/2.png" height="200px">

- Select the theme you want<br><br>
<img src="../../res/custom_themes/3.png" height="240px">


<br>

#### How to customize the theme

- Turn on the 'Use a custom theme' switch<br><br>
<img src="../../res/custom_themes/enable_custom.png" height="200px">

- Now you should see some new options. Use them to customize things<br><br>
<img src="../../res/custom_themes/custom_options.png" width="300px">


<br>

#### Import / Export custom themes

**Export**<br>
If you made a cool theme with this feature, you can share it with your Friends.<br>
Simply tap on 'Export the theme' option at the bottom. This should save the current custom theme to your phone's download folder as CSV file. File name should look like this<br>***Fedilab_color_export_yyyy-MM-dd-hh-mm-ss.csv***<br><br>
<img src="../../res/custom_themes/export.png" height="200px">


**Import**<br>
If someone else shared a nice theme, you can import and use it too. To do that
- First make sure 'Use a custom theme' switch is turned off. Otherwise you wouldn't be able to import a theme<br><br>
<img src="../../res/custom_themes/enable_custom.png" height="200px">

- Then tap on "Import a theme"<br><br>
<img src="../../res/custom_themes/import.png" height="200px">
