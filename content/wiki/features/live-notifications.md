*[Home](../../home) > [Features](../../features) &gt;* Live notifications

**Deprecated** See [push notifications](../../features/push-notifications)
<div align="center">
<img src="../../res/fedilab_icon.png" width="70px">
<b>Fedilab</b>
</div>

## Live notifications

If you're using the version 2.17.1 or a newer version of Fedilab, you might have noticed that there's a new notification with the title "Fedilab is running live notifications". Let's learn more about it and other notification modes in Fedilab.

### What it means
&nbsp;&nbsp;&nbsp;&nbsp;To show you the notifications in your phone from your fediverse account, Fedilab first needs to get them from your instance. Normally, the app would check with your instance for new notifications every 15 minutes and display them if there are new ones. But there is a problem. Since the app looks for new notifications every 15 minutes, you won't see them in your phone at the exact moment they become available. You'll get them only after Fedilab checks for it. Live notifications mean Fedilab will display notifications as soon as they become available.

### Why a persistent notification?
&nbsp;&nbsp;&nbsp;&nbsp;To give you live notifications, Fedilab needs to stay connected to your instance. Even in background, when you're not using Fedilab. In some phones, operating system automatically stops apps which are running in background. In these cases, the only way for Fedilab to keep itself running in background is to have a persistent notification. This is the reason for the new notification.

### Different notifications modes
There are three different notification modes in Fedilab.

1. **Live notifications**<br>
&nbsp;&nbsp;&nbsp;Fedilab will stay connected to your instance in background and notify you as soon as you have a new notification. But this method will drain your phone's battery more. This will display that persistent notification

2. **Live notifications delayed**<br>
&nbsp;&nbsp;&nbsp;If you select this one, Fedilab will be running in the background but won't stay connected to your instance. Instead it will check for new notifications every 30 seconds. You'll get your notifications quickly enough and the battery will drain less than above method. This will also display the persistent notification

3. **No live notifications**<br>
&nbsp;&nbsp;&nbsp;No live notifications means no live notifications :) Fedilab will check for notifications every 15 minutes and display them if there are new ones. You'll only get notifications every 15 minutes. For this mode you won't see the 'Fedilab is running ...' notification<a name="switch-mode"></a><br><br>

#### How to change notifications mode:

- open Fedilab

- Open the settings page from side menu / navigation drawer<br><br>
<img src="../../res/live_notifications/fs1.png">

- Swipe from the side to open the categories panel

- Select 'Notifications' section<br><br>
<img src="../../res/live_notifications/fs2.png">

- Press on the drop-down menu<br><br>
<img src="../../res/live_notifications/fs3.png">

- Then select the method you want<br><br>
<img src="../../res/live_notifications/fs4.png"><br><br>


### How to hide the persistent notification
There are two ways

1. [Hide the notification](#hide-notification)<br>
*This way, you won't see the notification. But the notification is still there, just hidden. Now you won't be annoyed by it and Fedilab will be able to stay running*

2. Turn off live notifications service<br>
*If you do this, you won't receive live notifications and "Fedilab is running live notifications" notification will be gone too*
<br><br>

<a name="hide-notification"></a><br>
#### Hide the notification
If you're using android 8 (Oreo) or newer version of Android, you can easily hide this notification. Here's how.

- Go to your system's settings<br><br>
<img src="../../res/live_notifications/ss1.png">

- Then open "Apps" section<br><br>
<img src="../../res/live_notifications/ss2.png">

- Now you'll see the list of apps installed on your phone

- Find "Fedilab" in the list, and click on it<br><br>
<img src="../../res/live_notifications/ss3.png">

- Now you'll see system settings for Fedilab

- Select "notifications" option<br><br>
<img src="../../res/live_notifications/ss4.png">

- Now you'll see different notification categories that Fedilab uses

- Find "Live notifications" and uncheck the box next to it<br><br>
<img src="../../res/live_notifications/ss5.png">

<br><br>
